package com.pirimid.learning.fruitvendor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FruitvendorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FruitvendorApplication.class, args);
    }

}
